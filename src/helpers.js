const timeTo24 = (time) => {
  const timeParts = time.split(':');
  const hour = timeParts[0];
  const period = timeParts[2].slice(-2);

  return (period === 'AM') ? parseInt(hour, 10) : (parseInt(hour, 10) + 12);
}

export const getGreeting = (time) => {
  if (!time) {
    return "What's good";
  }
  
  time = timeTo24(time);
  if (time < 12) {
    return 'Good morning'
  } else if (time > 17) {
    return 'Good evening'
  } else {
    return 'Good afternoon'
  }
}