import styled from 'styled-components';
import { Popup } from '../../styles/global-components';

export const TodoStyles = styled.div`
  grid-column: 3;
  grid-row: 3;
  justify-self: end;
  align-self: end;
`

export const TodoToggle = styled.div`
  cursor: pointer;
`
export const TodoPopup = styled(Popup)`
  min-width: 16rem;
  bottom: 7%;
  right: 2%;
  padding: 0.9em 1em;
`
export const TodoCount = styled.h3`
  margin-bottom: 0.9em;
`

export const TodoItems = styled.ul`
  list-style: none;
`