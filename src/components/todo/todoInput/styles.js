import styled from 'styled-components';

export const TodoInputStyles = styled.input.attrs({
  type: 'text',
  placeholder: props => props.placeholder || 'Enter new todo',
  value: props => props.value
})`
  border: none;
  font-size: 1em;
  background-color: transparent;
  color: white;
`;