import React from 'react';
import { TodoInputStyles } from './styles';

const TodoInput = ({ placeholder, value, onChange }, ) => (
  <TodoInputStyles value={value} placeholder={placeholder} onChange={onChange}/>
)

export default TodoInput;