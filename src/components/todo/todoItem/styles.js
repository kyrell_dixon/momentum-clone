import styled from 'styled-components';

export const Checkbox = styled.input.attrs({
  type: 'checkbox'
})`
  margin-right: 0.5em;
`

export const TodoItemStyles = styled.li`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin-bottom: 0.5em;

  :last-child {
    margin-bottom: 0.5em;
  }
`

export const TodoText = styled.span``