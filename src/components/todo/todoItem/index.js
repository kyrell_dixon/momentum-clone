import React from 'react';
import { TodoItemStyles, Checkbox, TodoText } from './styles';

const TodoItem = ({ todoText }) => (
  <TodoItemStyles>
    <Checkbox /><TodoText>{todoText}</TodoText>
  </TodoItemStyles>
)

export default TodoItem;