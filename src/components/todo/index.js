import React, { Component } from 'react';
import { TodoStyles, TodoToggle, TodoPopup, TodoCount, TodoItems } from './styles';
import TodoItem from './todoItem';
import TodoInput from './todoInput';

import Widget from '../widget';

class Todo extends Component {
  state = {
    value: '',
    todos: [],
    todoCount: 0
  }

  handleChange = (event) => {
    this.setState({ value: event.target.value });
  }

  handleSubmit = (event) => {
    const {value, todos, todoCount} = this.state;
    this.setState({
      value: '',
      todos: [...todos, value],
      todoCount: todoCount + 1
    })
    event.preventDefault();
  }

  renderTodos = () => {
    return this.state.todos.map((todo, i) => <TodoItem key={i} todoText={todo}/>)
  }

  render() {
    const { value, todoCount } = this.state;

    return (
      <TodoStyles>
        <Widget
          renderToggle={(toggleDisplay) => <TodoToggle onClick={toggleDisplay}>Todo</TodoToggle>}
          renderPopup={(toggled) =>
            <TodoPopup toggled={toggled} out={!toggled}>
              <TodoCount>{todoCount} to do</TodoCount>
              <TodoItems>
                {this.renderTodos()}
              </TodoItems>
              <form onSubmit={this.handleSubmit}>
                <TodoInput
                  placeholder="Enter new todo"
                  value={value}
                  onChange={this.handleChange}
                />
              </form>
            </TodoPopup>
          }
        />
      </TodoStyles>
    )
  }
}

export default Todo;