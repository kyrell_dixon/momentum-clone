import styled from 'styled-components';

export const BackgroundStyles = styled.div`
  width: 100%;
  height: 100%;
  position: absolute;
  z-index: -1;
`

export const BackgroundImage = styled.div`
  background-image: url('https://images.unsplash.com/photo-1521057248257-244c64e2f923?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjF9&s=8a716ef75ece1650ffa73c1fafb2849d');
  width: 100%;
  height: 100%;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  position: absolute;
`

export const Overlay = styled.div`
  width: 100%;
  height: 100%;
  position: absolute;
  background-color: rgba(0,0,0,0.3);
`