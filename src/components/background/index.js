import React from 'react';
import { BackgroundStyles, BackgroundImage, Overlay } from './styles';

const Background = () => {
  return (
    <BackgroundStyles>
      <BackgroundImage />
      <Overlay />
    </BackgroundStyles>
  )
}

export default Background;