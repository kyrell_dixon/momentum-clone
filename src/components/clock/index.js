import React, { Component } from 'react';
import { ClockStyles, Time, TimeNumber, TimePeriod } from './styles';

class Clock extends Component {

  componentDidMount = () => {
    this.timer = setInterval(this.props.updateTime, 60000);
    this.props.updateTime();
  }

  componentWillUnmount = () => {
    clearInterval(this.timer);
  }

  render() {
    const { time } = this.props;
    const period = time.slice(-2);
    const formattedTime = time.slice(0, -6);

    return (
      <ClockStyles>
        <Time>
          <TimeNumber>{formattedTime}</TimeNumber>
          <TimePeriod>{period}</TimePeriod>
        </Time>
      </ClockStyles>
    );
  }
}

export default Clock;