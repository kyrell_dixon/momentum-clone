import styled from 'styled-components';

export const ClockStyles = styled.div`
  grid-column: 2;
  grid-row: 2;
  margin: auto;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`
export const Time = styled.div`
  display: flex;
  flex-direction: row;
  align-items: baseline;
`

export const TimeNumber = styled.div`
  font-size: 8.5em;
  font-weight: 700;
`

export const TimePeriod = styled.span`
  font-size: 2em;
`