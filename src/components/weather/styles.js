import styled from 'styled-components';
import { Popup } from '../../styles/global-components';

export const WeatherStyles = styled.div`
  grid-column: 3;
  grid-row: 1;
  justify-self: end;
  align-self: start;
  cursor: pointer;
`;

export const WeatherDisplaySummary = styled.div`
  display: flex;
  flex-direction: row;
  align-items: baseline;
  font-size: 1.5em;
`;

export const WeatherIcon = styled.i`
  margin-right: 0.2em;
`;

export const WeatherPopup = styled(Popup)`
  display: flex;
  flex-direction: row;
  font-size: 1.3em;
  top: 9%;
  right: 2%;
  padding: 0.3em 0 0.2em 0;
`

export const CurrentTemp = styled.p``;
export const Location = styled.p``;
