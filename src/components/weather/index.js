import React, { Component } from 'react';
import axios from 'axios';

import Widget from '../widget';
import DailyWeather from './DailyWeather';
import { WeatherStyles, WeatherDisplaySummary, WeatherIcon, CurrentTemp, Location, WeatherPopup } from './styles';

const WEATHER_URL = `https://90mbn0tf3e.execute-api.us-east-1.amazonaws.com/dev/dark-sky/`;
const GEOCODING_URL = `https://90mbn0tf3e.execute-api.us-east-1.amazonaws.com/dev/google-maps?latlng=`;

class Weather extends Component {
  state = {
    weather: null,
    coords: {
      latitude: 40.71422,
      longitude: -73.96145
    },
    location: null
  }

  fetchLocation = async (coords) => {
    const cacheKey = `location-${coords.latitude},${coords.lng}`;
    const cachedHits = localStorage.getItem(cacheKey);

    if (cachedHits) {
      console.log("Location cached");
      this.setState({ location: JSON.parse(cachedHits) });
      return;
    }

    console.log("location not cached")
    const url = `${GEOCODING_URL}${coords.latitude},${coords.longitude}`;
    const res = await axios.get(url);
    
    const locationData = res.data.results[3];
    const location = locationData.address_components;

    this.setLocationCache(location, cacheKey);
  }

  fetchWeather = async (coords) => {

    const cacheKey = `weather-${coords.latitude},${coords.longitude}`;
    const cachedHits = localStorage.getItem(cacheKey);
    
    if (cachedHits) {
      console.log("Weather cached");
      this.setState({ weather: JSON.parse(cachedHits) });
      return;
    }
    console.log("weather not cached");
    const url = `${WEATHER_URL}${coords.latitude},${coords.longitude}`;

    const res = await axios.get(url);
    const weather = res.data;
    this.setWeatherCache(weather, cacheKey);
  }

  setWeatherCache = (data, key) => {
    localStorage.setItem(key, JSON.stringify(data));
    this.setState({ weather: data });
  }

  setLocationCache = (data, key) => {
    localStorage.setItem(key, JSON.stringify(data));
    this.setState({ location: data });
  }

  setCoords = async (position) => {
    const coords = position.coords || this.state.coords;

    // using the unary + to convert to number
    const looseLat = +coords.latitude.toFixed(3);
    const looseLng = +coords.longitude.toFixed(3);

    const looseCoords = {
      latitude: looseLat,
      longitude: looseLng
    }

    this.setState({coords: looseCoords});
    this.fetchLocation(looseCoords);
    this.fetchWeather(looseCoords);
  }

  componentDidMount = () => {

    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.setCoords(position);
      });
      return;
    }
    this.fetchWeather(this.state.coords);
    this.fetchLocation(this.state.coords);
  }

  render() {
    if (!this.state.weather || !this.state.location) {
      return <WeatherStyles>Loading...</WeatherStyles>
    }

    const { weather, location } = this.state;
    const { currently } = weather;
    const currentTemp = weather.currently.temperature;
    // console.log("Current weather is", weather.currently);
    // console.log("Daily weather is", weather.daily.data.slice(0,5));
    // console.log("Location is", location);
    
    return (
      <Widget 
        renderToggle={(toggleDisplay) => 
          <WeatherStyles >
            <WeatherDisplaySummary onClick={toggleDisplay}>
              <WeatherIcon className={`wi wi-forecast-io-${currently.icon}`} />
              <CurrentTemp>{Math.round(currentTemp)}°</CurrentTemp>
            </WeatherDisplaySummary>
            <Location onClick={toggleDisplay}>{location[0].long_name}</Location>
          </WeatherStyles>}
        renderPopup={(toggled) => 
          <WeatherStyles>
            <WeatherPopup toggled={toggled} out={!toggled}>
              <DailyWeather weather={weather.daily.data.slice(0,5)}/>
            </WeatherPopup>
          </WeatherStyles>}
      />
    )
  }
}

export default Weather;