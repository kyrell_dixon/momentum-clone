import React, { Fragment } from 'react';
import { WeatherItem, HighTemp, LowTemp, DayLabel, DayWeather } from './styles';
import { WeatherIcon } from '../styles';

const DailyWeather = ({ weather }) => {

  const days = ['Sun', 'Mon', 'Tues', 'Wed', 'Thurs', 'Fri', 'Sat'];
  
  return (
    <Fragment>
      {weather.map((item, i) => {
        return (
          <DayWeather key={i}>
            <DayLabel>{days[i % 7]}</DayLabel>
            <WeatherItem>
              <WeatherIcon className={`wi wi-forecast-io-${item.icon}`} />
              <HighTemp>{Math.round(item.temperatureHigh)}</HighTemp>
              <LowTemp>{Math.round(item.temperatureLow)}</LowTemp>
            </WeatherItem>
          </DayWeather>
        )
      })}
    </Fragment>
  )
}

export default DailyWeather;