import styled from 'styled-components';

export const WeatherItem = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  font-size: 0.8em;
  padding: 0.3em;
`

export const DayWeather = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`

export const HighTemp = styled.p`
  margin-right: 0.1em;
`;

export const LowTemp = styled.p`
  color: grey;
  font-size: 0.7em;
`;

export const DayLabel = styled.p`
  font-size: 0.6em;
`