import React, { Component } from 'react';
import GreetingInput from './greetingInput';
import { GreetingStyles } from './styles';
import { getGreeting } from '../../helpers';

class Greeting extends Component {
  state = {
    nameSubmit: false
  }

  setSubmit = () => {
    this.setState({ nameSubmit: true });
  }

  render() {
    const { time, username, onChange, updateUsername } = this.props;

    if (this.state.nameSubmit) {
      return <GreetingStyles>{getGreeting(time)}, {username}</GreetingStyles>
    }
    return (
      <GreetingStyles>
        <form onSubmit={(event) => {
          updateUsername(event);
          this.setSubmit();
        }}>
          <GreetingInput
            value={username}
            placeholder="What is your name?"
            onChange={onChange}
          />
        </form>
      </GreetingStyles>
    )
  // }
  }
}

export default Greeting;