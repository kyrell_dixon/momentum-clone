import styled from 'styled-components';

export const GreetingStyles = styled.div`
  font-size: 2em;
  grid-column: 2;
  grid-row: 2;
  align-self: self-end;
  margin: 0 auto 20%;
`