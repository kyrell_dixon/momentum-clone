import styled from 'styled-components';

export const GreetingInputStyles = styled.input.attrs({
  type: 'text',
  placeholder: props => props.placeholder || 'Type your name',
  value: props => props.value
}) `
  border: none;
  font-size: 1em;
  background-color: rgba(0,0,0,0.8);
  text-align: center;
  color: white;
`;