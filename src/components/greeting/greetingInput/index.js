import React from 'react';
import { GreetingInputStyles } from './styles';

const GreetingInput = ({ placeholder, username, onChange }, ) => (
  <GreetingInputStyles value={username} placeholder={placeholder} onChange={onChange}/>
)

export default GreetingInput;