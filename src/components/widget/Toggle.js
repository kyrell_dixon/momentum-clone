import React, { Fragment } from 'react';

const Toggle = ({ children }) => (
    <Fragment>
      {children}
    </Fragment>
)

export default Toggle;