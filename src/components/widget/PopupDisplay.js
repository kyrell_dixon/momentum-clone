import React, { Fragment } from 'react';

const PopupDisplay = ({ children }) => (
  <Fragment>
    {children}
  </Fragment>
)

export default PopupDisplay;