import React, { Component, Fragment } from 'react';
import Toggle from './Toggle';
import PopupDisplay from './PopupDisplay';

class Widget extends Component {
  state = {
    toggled: false
  }

  toggleDisplay = () => {
    this.setState({toggled: !this.state.toggled});
  }

  render() {
    const {renderToggle, renderPopup, children} = this.props;

    return (
      <Fragment>
        <Toggle>{renderToggle && renderToggle(this.toggleDisplay)}</Toggle>
        <PopupDisplay>{renderPopup && renderPopup(this.state.toggled)}</PopupDisplay>
        {children}
      </Fragment>
    )
  }
}

export default Widget;