import styled from 'styled-components';
import { Popup } from '../../styles/global-components';

export const SettingsStyles = styled.div`
  grid-column: 1;
  grid-row: 3;
  justify-self: start;
  align-self: end;
  cursor: pointer;
`

export const SettingsPopup = styled(Popup)`
  bottom: 7%;
  left: 2%;
`

export const SettingsToggle = styled.p``