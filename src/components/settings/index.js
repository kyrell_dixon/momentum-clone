import React from 'react';
import Widget from '../widget';
import { SettingsStyles, SettingsPopup, SettingsToggle } from './styles';

const Settings = (props) => {
  return (
    <SettingsStyles>
      <Widget
        renderToggle={(toggleDisplay) => <SettingsToggle onClick={toggleDisplay}>Settings</SettingsToggle>}
        renderPopup={(toggled) => 
        <SettingsPopup toggled={toggled} out={!toggled}>
          <h1>Settings Details</h1>
          <button onClick={props.toggleClockDisplay}>Toggle Clock</button>
          <button onClick={props.toggleTodoDisplay}>Toggle Todo</button>
          <button onClick={props.toggleWeatherDisplay}>Toggle Weather</button>
          <button onClick={props.toggleGreetingDisplay}>Toggle Greeting</button>
        </SettingsPopup>}
      />
    </SettingsStyles>
  )
}

export default Settings;