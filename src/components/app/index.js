import React, { Component } from 'react';
import Background from '../background';
import Clock from '../clock';
import Greeting from '../greeting';
import Weather from '../weather';
import Todo from '../todo';
import Settings from '../settings';

import { AppStyles } from './styles';

class App extends Component {
  state = {
    username: '',
    time: '',
    clockDisplayed: true,
    weatherDisplayed: true,
    todoDisplayed: true,
    greetingDisplayed: true
  }

  updateTime = () => {
    let time = new Date().toLocaleTimeString();
    this.setState({ time });
  }

  updateUsername = (event) => {
    const { username } = this.state;
    this.setState({ username });
    event.preventDefault();
  }

  handleUsernameChange = (event) => {
    this.setState({ username: event.target.value });
  }

  toggleClockDisplay = () => {
    this.setState({clockDisplayed: !this.state.clockDisplayed});
  }
  toggleWeatherDisplay = () => {
    this.setState({weatherDisplayed: !this.state.weatherDisplayed});
  }
  toggleTodoDisplay = () => {
    this.setState({todoDisplayed: !this.state.todoDisplayed});
  }
  toggleGreetingDisplay = () => {
    this.setState({greetingDisplayed: !this.state.greetingDisplayed});
  }
  
  render() {
    return (
      <AppStyles>
        <Background />
        {this.state.clockDisplayed && <Clock updateTime={this.updateTime} time={this.state.time}/>}
        {this.state.weatherDisplayed && <Weather />}
        {this.state.todoDisplayed && <Todo />}
        {this.state.greetingDisplayed && 
          <Greeting
            time={this.state.time}
            username={this.state.username}
            updateUsername={this.updateUsername}
            onChange={this.handleUsernameChange}
          />}
        <Settings 
          toggleClockDisplay={this.toggleClockDisplay}
          toggleWeatherDisplay={this.toggleWeatherDisplay}
          toggleTodoDisplay={this.toggleTodoDisplay}
          toggleGreetingDisplay={this.toggleGreetingDisplay}
        />
      </AppStyles>
    );
  }
}

export default App;