import styled from 'styled-components';
import { fadeIn, fadeOut } from './style-utils';

export const Popup = styled.div`
  display: block;
  position: absolute;
  background-color: rgba(0,0,0,0.7);
  visibility: ${props => props.out ? 'hidden' : 'visible'};
  animation: ${props => props.out ? fadeOut : fadeIn} 0.3s linear;
  transition: visibility 0.3s linear;
  min-width: 10rem;
  min-height: 3rem;
`