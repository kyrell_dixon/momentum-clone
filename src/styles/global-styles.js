import { injectGlobal } from 'styled-components';
import '../assets/css/weather-icons.min.css';

injectGlobal`
  * {
    box-sizing: border-box;
    margin: 0;
    padding: 0;
  }
  
  html, body, #root {
    margin: 0;
    padding: 0;
    font-family: sans-serif;
    height: 100%;
    width: 100%;
    color: white;
    font-size: 1em;
  }
`